<?php

/**
 * Send_email
 *
 * @package	CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author	
 * @link	
 */
class Send_email
{
    private $_CI;
	
    public function __construct()
    {
    	$this->_CI = & get_instance();
	}
	
	public function send($config,$data = array())
	{
	 	$this->_CI->load->library('email');
		$config_email['mailtype'] = "html";
		// $config_email['protocol'] = "smtp";
		// $config_email['smtp_host'] = "localhost";
		 
		$body = $this->_CI->load->view($config['template'],$data, TRUE);
		$subject = $config['subject'];
		
		if ( ! empty ($data['replace']))
		{
			foreach ($data['replace'] as $key => $replace)
			{
				$body = str_replace($key,$replace,$body);
				$subject = str_replace($key,$replace,$subject);
			}
		}
		
		$this->_CI->email->initialize($config_email);
	
		$this->_CI->email->clear();
		$this->_CI->email->to($data['email']);
		
		if (empty($data['from_email']))
		{
			$data['from_email'] = $config['from'];
		}
		
		$this->_CI->email->from($data['from_email'],$config['from_name']);
		$this->_CI->email->subject($subject);
		$this->_CI->email->message($body);

		if($this->_CI->email->send())
		{
			return true.'_';
		}
		else
		{
			return false;
		}
	}
}


/* End of file send_email.php */
/* Location: ./application/libraries/send_email.php */


